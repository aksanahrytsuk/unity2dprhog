using System;
using UnityEngine;
using DG.Tweening;
[RequireComponent(typeof(SpriteRenderer))]
public class DisappearComponent : EffectComponent
{
  [SerializeField] private float _disappearDuration;
  SpriteRenderer _spriteRenderer;
  void Awake()
  {
    _spriteRenderer = GetComponent<SpriteRenderer>();
  }
  public override void DoEffect(Action _callback) => _spriteRenderer.DOFade(0f, _disappearDuration).OnComplete(() =>
  {
    _callback?.Invoke();
  });
}
