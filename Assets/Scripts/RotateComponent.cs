using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class RotateComponent : EffectComponent
{
  [SerializeField] private float _duration;
  public override void DoEffect(Action _callback) => transform.DORotate(new Vector3(0,0,360), 2, RotateMode.LocalAxisAdd).OnComplete(() =>
  {
    gameObject.SetActive(false);
    _callback?.Invoke();
  });
}
