using System;
using UnityEngine;
using DG.Tweening;

//RequireComponent[tipeof(SpriteRenderer)]
public class ScaleComponent : EffectComponent
{
  [SerializeField] private float _scale_Multiplier;
  [SerializeField] private float _scaleDuration;

  public override void DoEffect(Action _callback) => transform.DOScale(transform.localScale * _scale_Multiplier, _scaleDuration).OnComplete(() =>

                                         {
                                           gameObject.SetActive(false);
                                           _callback?.Invoke();
                                         });
}

