using System;
using UnityEngine;


public class GameItem : MonoBehaviour
{
  EffectComponent _effectComponent;
  public event Action OnFind;

  void Awake()
  {
    _effectComponent = GetComponent<EffectComponent>();
  }
  private void OnMouseUpAsButton() //объект кликабельный
  {
    OnFindMethod();
  }

  public void OnFindMethod() => _effectComponent.DoEffect(() =>
                                 {
                                   OnFind?.Invoke(); //вызов ивента}
                                 });
}
