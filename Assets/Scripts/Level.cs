using System;
using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour
{
  private List<GameItem> _items = new List<GameItem>();
  public event Action OnComplit;
  private int _itemsCount;

  public void Initialize()
  {
    GameItem[] gameItems = GetComponentsInChildren<GameItem>(); // Поиск сомпонентов gameItem в Level возвращает массив всех найденых объектов
    _items.AddRange(gameItems); // массив найденых объетов добавляем в лист 
    _itemsCount = _items.Count;

    for (int i = 0; i < _items.Count; i++)
    {
      _items[i].OnFind += OnItemFinded; //объекты , которые у нас есть, подписали на метод OnItemFinded
    }
  }
  private void OnItemFinded()
  {
    _itemsCount--;

    if (_itemsCount == 0)
    {
      OnComplit?.Invoke();
    }
  }
}
