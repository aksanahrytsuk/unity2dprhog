using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
  [SerializeField] private List<Level> _levels = new List<Level>();
  private Level _currentLevel;
  private int _levelIndex;

  void Awake()
  {
    CreateLevel();
  }

  void Start()
  {
    StartGame();
  }
  private void CreateLevel()
  {
    if (_currentLevel != null)
    {
      Destroy(_currentLevel.gameObject);
      _currentLevel = null;
    }

    int index = _levelIndex;
    if (_levelIndex >= _levels.Count)
    {
      index = _levelIndex % _levels.Count;
    }
    _currentLevel = Instantiate(_levels[index].gameObject).GetComponent<Level>();
  }

  private void StartGame()
  {
    _currentLevel.OnComplit += StopGame;
    _currentLevel.Initialize();
  }

  private void StopGame()
  {
    _levelIndex++;
    CreateLevel();
    StartGame();
  }
}
